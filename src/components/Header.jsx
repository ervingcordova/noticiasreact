import React from  'react'

const Header = ()=>{
    return(
        <nav className="navbar navbar-dark bg-primary justify-content-center">
          <h1 className="text-light">Noticias desde API</h1>
        </nav>
    )
}

export default Header