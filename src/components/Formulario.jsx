import React, { Component } from 'react';
import Select from 'react-select';
class Formulario extends Component {
    state = {
        selectedOption: null,
    };
    handleChange = selectedOption => {
        this.setState(
            { selectedOption },
            this.sendCategory
        );
    };
    componentDidMount(){
        this.setState({selectedOption: this.props.categorieDefaul})
    }
    sendCategory = ()=>{
        this.props.selectCategory(this.state.selectedOption)
    }
    render() {
        const {categories} = this.props;
        const {selectedOption} = this.state;
        return (
            <form action="">
                <div className="form-row justify-content-center">
                    <div className="form-group col-md-4">
                        <Select
                            name="form-field-name"
                            value={selectedOption}
                            options={categories}
                            onChange={this.handleChange}
                        />
                    </div>
                </div>
            </form>
        );
    }
}

export default Formulario;