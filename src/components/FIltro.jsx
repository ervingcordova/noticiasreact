import React, { Component } from 'react';

class FIltro extends Component {
    
    handleChange = (e)=>{
        this.props.textTitle(e.target.value);
    }
    
    render() {
        return (
            <form action="">
                <div className="form-row justify-content-center">
                    <div className="form-group col-md-4">
                        <input 
                            type="text" 
                            className="form-control" 
                            onChange={this.handleChange} 
                            placeholder="Busqueda por nombre" />
                    </div>
                </div>
            </form>
        );
    }
}

export default FIltro;