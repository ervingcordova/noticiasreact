import React, { Component } from 'react';
import Noticia from './Noticia';
class ListaNoticias extends Component {
    render() {
        const {news} = this.props;
        return (
            <div className="card-columns">
                {
                    news.map((notice, index)=>{
                        return <Noticia key={index} dataNew={notice} />
                    })
                }
            </div>
        );
    }
}

export default ListaNoticias;