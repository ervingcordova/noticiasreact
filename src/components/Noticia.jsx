import React, { Component } from 'react';
class Noticia extends Component {
    render() {
        const {dataNew} = this.props
        return (
            <div className="card" style={{maxWidth: '18em'}}>
                <img className="card-img-top" src={dataNew.urlToImage} alt={dataNew.title}/>
                <div className="card-body">
                    <h5 className="card-title">{dataNew.title}</h5>
                    <p className="card-text">Descripción: {dataNew.description}</p>
                </div>
                <div className="card-footer">
                    <a target="blank" href={dataNew.url} className="btn btn-primary btn-lg active">Ver noticia</a>
                </div>
            </div>
        );
    }
}

export default Noticia;