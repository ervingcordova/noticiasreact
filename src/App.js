import React from 'react';
import './App.css';
import Header from './components/Header';
import Formulario from './components/Formulario';
import ListaNoticias from './components/ListaNoticias';
import FIltro from './components/FIltro';
const categoriesNews = [
  {
    value: 'business',
    label: 'Negocios'
  },
  {
    value: 'entertainment',
    label: 'Entretenimiento'
  },
  {
    value: 'general',
    label: 'General'
  },
  {
    value: 'health',
    label: 'Salud'
  },
  {
    value: 'science',
    label: 'Ciencia'
  },
  {
    value: 'sports',
    label: 'Deporte'
  },
  {
    value: 'technology',
    label: 'Tecnología'
  }
]

class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      listNews: [],
      categoriesNews: categoriesNews,
      categorySelect: categoriesNews[5]
    }
  }
  componentDidMount(){
    this.consultarNoticia(this.state.categorySelect.value);
  }
  /*Get API Noticias por Categoria*/
  consultarNoticia = async (category)=>{
    const urlApi = `https://newsapi.org/v2/top-headlines?country=mx&category=${category}&apiKey=a84009a4d8944d9b800fe588d4a0384e`;
    const respuesta = await fetch(urlApi);
    const noticias = await respuesta.json();
    this.setState({listNews: noticias.articles})
  }
  /*Función para el PROPS selectCategory*/
  /*Buscar noticia por categoria*/
  findNewsByCategory = (category)=>{
    this.consultarNoticia(category.value)
    this.setState({categorySelect: category})
  }
  /*Filtrar noticia por Título*/
  newsByTitle = (title)=>{
    if(title === ''){
      this.consultarNoticia(this.state.categorySelect.value)
    }else{
      var updateNews = this.state.listNews;
      updateNews = updateNews.filter((item =>{
          return (item.title).toLowerCase().search(title.toLowerCase()) !== -1;
      }) );
      this.setState({listNews: updateNews})
    }
  }
  /*Función para el PROPS textTitle*/
  findNewsByTitle = (title)=>{
    this.newsByTitle(title);
  }
  render(){
    return(
      <div className="bg-dark">
        <Header></Header>
        <section className="container bg-light">
          <br></br>
          <h4 className="text-center">Encuentra noticias por categorías deploy con NETIFY</h4>
          <br></br>
          <Formulario 
            categorieDefaul={this.state.categorySelect}
            categories={this.state.categoriesNews}
            selectCategory={this.findNewsByCategory}
          />
          <FIltro
            textTitle={this.findNewsByTitle}
          />
          <br></br>
          <ListaNoticias
            news={this.state.listNews}
          />
        </section>
      </div>
    )
  }
}

export default App;
